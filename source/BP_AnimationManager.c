////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Animation Manager
//
// summary:	A simple animation manager that will run an animation
//			from an animation structure
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "BP_AnimationManager.h"

bool BP_RunAnimation(unsigned int* Src, unsigned int AnimationTime, unsigned short* OBJAddress, AnimationStruct* Animation)
{
	//will give the current elapsed time between the current time and the time
	//the animation started
	u32 ElapsedTime = Frame_Count - AnimationTime;

	u16 i = 0;
	u32 FramePassed = Animation->FrameDuration;

	//this loop will compare the frames passed with the current elapsed time
	//if the elapsed time is 20 and each animation frame is equal to 5 frames
	//then it should be 4 since 0-4 was frame 0 5-9 frame 1 10-14 frame 2
	//15-19 frame 3 20-24 frame 4
	for (i = 0; FramePassed <= ElapsedTime; i++)
		FramePassed+=Animation->FrameDuration;

	//check to see if the max frames has been reached
	if (i >= Animation->Frames)
			return true;

	FramePassed = (Animation->startAdd << 3);

	FramePassed += (Animation->TileCount << 3) * i;  



	//load the animation into the OBJ address
	BP_memcpy32(&Src[FramePassed], OBJAddress, (Animation->TileCount << 3));

	return false;
}