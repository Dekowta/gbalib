#include "BP_Graphics.h"


void BP_OBJmemcpy32(unsigned int* src, unsigned short count, unsigned short OBJAdd)
{
	//sets the destination to be the OBJ memory 
	unsigned int* dst = OBJAdd32[OBJAdd];
	while(count--)
		*dst++ = *src++;
}

void BP_OBJmemcpy16(unsigned short* src, unsigned short count, unsigned short OBJAdd)
{
	//sets the destination to be the OBJ memory 
	short int* dst = OBJAdd16[OBJAdd];
	while(count--)
		*dst++ = *src++;
}

void BP_InitaliseOAM(SpriteAtt* src, unsigned short count)
{

	unsigned short OAMcount = count;
	//// This sets the struct to be accessed in 32bit chunks since
	//// each member of the struct is 16bits then the are doing
	//// 2 at the same time. Since this is just initalisation
	//// and all the data apart from sprite hidden needs to be 0
	//// we can do it like this
	//unsigned int* dst = (unsigned int*)src;
	int i = 0;
	while(count--)
	{
		//*dst++ = 512;
		//*dst++ = 0;
		//The above should work but for some reason it doesnt
		//This method however works fine 
		src[i].Att0 = 512;
		i++;
	}

	//copies over all the OAM/OAM's that have just been initalised
	BP_OAMCpy(OAMAddress, src, OAMcount);
}

void BP_OAMCpy(SpriteAtt* dst, SpriteAtt* src, unsigned short count)
{
	// apparently this can screw up on a few compilers so if there is an issue with
	// the OAM's not working its likely its here below describes how to fix this
	////while(count--)
	////{
	////	*dst++ = *src++;
	////}

	// BELLOW WAS CORRECT DO NOT USE THE ABOVE METHOD EVEN THOUGH IT IS
	// VALID

	//=======================================================
	// to fix the issue with the OAM not copying over correctly then
	// the data will need to be split up into 32bit chunks
	// to create this fix just uncomment the code after this and comment out the code
	// before.

	unsigned int* dst32 = (unsigned int*)dst;
	unsigned int* src32 = (unsigned int*)src;

	while(count--)
	{
		// since we are now copying by 32bits we will need to perform the copy operation
		// twice
		*dst32++ = *src32++;
		*dst32++ = *src32++;
	}
	//=======================================================
}


void BP_SetOAMPos(SpriteAtt* src, unsigned char y, unsigned short x)
{
	BitCSet(src->Att0, OAMATT0_Y_Shift, OAMATT0_Y_Mask, y);
	BitCSet(src->Att1, OAMATT1_X_Shift, OAMATT1_X_Mask, x);
}


void BP_EnableOAM(SpriteAtt* src, bool Enable)
{
	if (Enable)
	{
		// If the sprite is disabled then the bit needs to be 0
		// so all that needs to be done is clear the bit
		BitClr(src->Att0, OAMATT0_Enable_Shift, OAMATT0_Enable_Mask);
	}
	else
	{
		// If the bit is already 0 or 1 then it is set to 1 if the
		// sprite is enabled so no need to clear it
		BitSet(src->Att0, OAMATT0_Enable_Shift, 1);
	}
}

void BP_OAMFlipV(SpriteAtt* src, bool Flip)
{
	if (Flip)
	{
		// If the sprite is disabled then the bit needs to be 0
		// so all that needs to be done is clear the bit
		BitClr(src->Att1, OAMATT1_VFlip_Shift, OAMATT1_VFlip_Mask);
	}
	else
	{
		// If the bit is already 0 or 1 then it is set to 1 if the
		// sprite is enabled so no need to clear it
		BitSet(src->Att1, OAMATT1_VFlip_Shift, 1);
	}
}

void BP_OAMFlipH(SpriteAtt* src, bool Flip)
{
	if (Flip)
	{
		// If the sprite is disabled then the bit needs to be 0
		// so all that needs to be done is clear the bit
		BitClr(src->Att1, OAMATT1_HFlip_Shift, OAMATT1_HFlip_Mask);
	}
	else
	{
		// If the bit is already 0 or 1 then it is set to 1 if the
		// sprite is enabled so no need to clear it
		BitSet(src->Att1, OAMATT1_HFlip_Shift, 1);
	}
}

void BP_SetSprite(SpriteAtt* src, unsigned short Tile, unsigned short Pallet)
{
	BitCSet(src->Att2, OAMATT2_TID_Shift, OAMATT2_TID_Mask, Tile);
	BitCSet(src->Att2, OAMATT2_Pallet_Shift, OAMATT2_Pallet_Mask, Pallet);
}

void BP_SetOAM (SpriteAtt* src, unsigned short Att0, unsigned short Att1, unsigned short Att2)
{
	src->Att0 = Att0;
	src->Att1 = Att1;
	src->Att2 = Att2;
	src->Att3Padding = 0;
}


void BP_RenderScreenBase(unsigned short* src,
						 unsigned short x,
						 unsigned short y,
						 unsigned short Tile,
						 unsigned short Pallet,
						 unsigned short Vflip,
						 unsigned short Hflip)
{
	src[(y*32) + x] = Set_BG(Tile, Vflip, Hflip, Pallet);
}


//Note for the future dont change this to a pointer
//it seems to be slower than doing it this method
//Not what I expected since the the data is stored in IWRAM any way
//so you are accress the fastest memory type
void BP_RenderMap(MapAtt MapToRender)
{
	//Loop Through the map
	unsigned int x = 0;
	unsigned int y = 0;

	unsigned int x2 = 0;
	unsigned int y2 = 0;

	for (y = MapToRender.CurrentY; y < MapToRender.CurrentY + 32; y++)
	{
		for (x = MapToRender.CurrentX; x <  MapToRender.CurrentX + 32; x++)
		{
			
			//get the data out of the map address
			unsigned short Tile	  = (MapToRender.MapSrc[(x * MapToRender.MapHeight) + y]  & (255 << 0));
			unsigned short Vflip  = ((MapToRender.MapSrc[(x * MapToRender.MapHeight) + y]  & (1 << 8)) >> 8);
			unsigned short Hflip  = ((MapToRender.MapSrc[(x * MapToRender.MapHeight) + y]  & (1 << 9)) >> 9);
			unsigned short Pallet = ((MapToRender.MapSrc[(x * MapToRender.MapHeight) + y]  & (1 << 10)) >> 10);

			//render the data to the back buffer screen base
			BP_RenderScreenBase(&MapToRender.ScreenBase[0], x2, y2, (Tile + MapToRender.TileOffSet), Pallet, Vflip, Hflip);
			x2++;
		}
		x2 = 0; 
		y2++;
	}

}


void BP_BlockIn()
{
	u32 StartTime = Frame_Count;

	bool Skip = false;

	while(Skip == false)
	{
		if(Frame_Count < StartTime + 5)
			MOSAICAdd = (1 << 4) | 1;
		if(Frame_Count < StartTime + 10 && Frame_Count >= StartTime + 5)
			MOSAICAdd = (2 << 4) | 2;
		if(Frame_Count < StartTime + 15 && Frame_Count >= StartTime + 10)
			MOSAICAdd = (3 << 4) | 3;
		if(Frame_Count < StartTime + 20 && Frame_Count >= StartTime + 15)
			MOSAICAdd = (4 << 4) | 4;
		if(Frame_Count < StartTime + 25 && Frame_Count >= StartTime + 20)
			MOSAICAdd = (5 << 4) | 5;
		if(Frame_Count < StartTime + 30 && Frame_Count >= StartTime + 25)
			MOSAICAdd = (6 << 4) | 6;
		if(Frame_Count < StartTime + 35 && Frame_Count >= StartTime + 30)
		{
			MOSAICAdd = (7 << 4) | 7;
			Skip = true;
		}

		BP_VBlankWait();
	}
}

void BP_BlockOut()
{
	u32 StartTime = Frame_Count;

	bool Skip = false;

	while(Skip == false)
	{
		if(Frame_Count < StartTime + 5)
		{
			MOSAICAdd = (7 << 4) | 7;
		}
		if(Frame_Count < StartTime + 10 && Frame_Count >= StartTime + 5)
		{
			MOSAICAdd = (6 << 4) | 6;
		}
		if(Frame_Count < StartTime + 15 && Frame_Count >= StartTime + 10)
		{
			MOSAICAdd = (5 << 4) | 5;
		}
		if(Frame_Count < StartTime + 20 && Frame_Count >= StartTime + 15)
		{
			MOSAICAdd = (4 << 4) | 4;
		}
		if(Frame_Count < StartTime + 25 && Frame_Count >= StartTime + 20)
		{
			MOSAICAdd = (3 << 4) | 3;
		}
		if(Frame_Count < StartTime + 30 && Frame_Count >= StartTime + 25)
		{
			MOSAICAdd = (2 << 4) | 2;
		}
		if(Frame_Count < StartTime + 35 && Frame_Count >= StartTime + 30)
		{
			MOSAICAdd = (1 << 4) | 1;
		}
		if(Frame_Count < StartTime + 40 && Frame_Count >= StartTime + 35)
		{
			MOSAICAdd = 0;
			Skip = true;
		}
		BP_VBlankWait();
	}
}