////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw IRQ 
//
// summary:	This will assist with setting up and managing the irqs and irs table
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "BP_Irq.h"

IRQ_irs IRS_Table[13];


//This has been taken from tonc's irq library for IRQ allong with
//many of the methods that will be found in the IRQ functions/functionality
static const IRQ_Sender IRQ_Senders[] = 
{
	{ 0x0004, 0x0008 },		// REG_DISPSTAT,	DSTAT_VBL_IRQ
	{ 0x0004, 0x0010 },		// REG_DISPSTAT,	DSTAT_VHB_IRQ
	{ 0x0004, 0x0020 },		// REG_DISPSTAT,	DSTAT_VCT_IRQ
	{ 0x0102, 0x0040 },		// REG_TM0CNT,		TM_IRQ
	{ 0x0106, 0x0040 },		// REG_TM1CNT,		TM_IRQ
	{ 0x010A, 0x0040 },		// REG_TM2CNT,		TM_IRQ
	{ 0x010E, 0x0040 },		// REG_TM3CNT,		TM_IRQ
	{ 0x0128, 0x4000 },		// REG_SIOCNT		SIO_IRQ
	{ 0x00BA, 0x4000 },		// REG_DMA0CNT_H,	DMA_IRQ>>16
	{ 0x00C6, 0x4000 },		// REG_DMA1CNT_H,	DMA_IRQ>>16
	{ 0x00D2, 0x4000 },		// REG_DMA2CNT_H,	DMA_IRQ>>16
	{ 0x00DE, 0x4000 },		// REG_DMA3CNT_H,	DMA_IRQ>>16
	{ 0x0132, 0x4000 },		// REG_KEYCNT,		KCNT_IRQ
	{ 0x0000, 0x0000 },		// cart: none
};

void BP_InitIRQ()
{
	//The first step to turn all IRQ's off
	//this will ensure that no IRQ's happen while this function
	//is called though its very unlikely in the first place any way
	IRQ_IME = 0;

	//next make sure that the irs table is clear
	//a bit of a cheap way to do it but it will ensure that it is all cleared
	int i = 0;

	for(i = 0; i < 13; i++)
	{
		IRS_Table[i].flag	= 0;
		IRS_Table[i].irs	= 0;
	}

	//unlike toncs function I wont be allowing for an override for the master irs
	//so the master will just be loaded
	IRQ_ISR_MAIN = (fnptr)IRS_Master;

	//now that has been done and the master IRS will running not matter what irq has
	//triggered. The IRQ's are re-enabled

	IRQ_IME = 1;
}


void BP_EnableIRQ(enum IRQ_Index IRQ_ID, fnptr isr)
{
	//First Disable IRQ's to make sure that none happen while an IRQ is being set
	IRQ_IME = 0;

	IRQ_irs* IRS_T = IRS_Table;
	//first enable the IRQ
	const IRQ_Sender* sender = &IRQ_Senders[IRQ_ID];

	//will set the IRQ sender register that is related to the IRQ
	//so for the vblank the DISPSTAT register also needs to be set
	//the 3rd bit to 1 so it can trigger Vblank IRQ
	*(unsigned short*)(REG_BASE + sender->IRQ_Reg1Off) |= sender->IRQ_Reg2Flag;
	//Next the IE regiester need to be set to enable the IRQ
	IRQ_IE |= (1 << IRQ_ID);

	//The IRS Table is going to be stored in no real order, though it
	//could be stored with the IRQ_ID setting where the irs is
	//placed in the table however it may be best just to position it
	//in the order it was added
	int i = 0;

	for (i = 0; IRS_T[i].flag; i++)
		if (IRS_T[i].flag == IRQ_ID)
			break;

	//Set the irs in the table
	IRS_T[i].irs  = isr;
	IRS_T[i].flag = (1 << IRQ_ID);

	//reset the master to 1 so that IRQ's can be enabled again
	IRQ_IME = 1;

}

void BP_VBlankWait()
{
	asm("swi 0x05");
}