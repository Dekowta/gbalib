////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Animation Manager
//
// summary:	A simple animation manager that will run an animation
//			from an animation structure
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "BP_Input.h"

inline bool BP_isKeyPressed(enum Keys Key_Flag)
{
	if (Key_Data & Key_Flag)
	{
		return false;
	}
	else
	{
		return true;
	}

}


inline void BP_UpdateKeys(void)
{
	Key_Data = 0;
	Key_Data = KEY_Input;
}