////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Text Writer
//
// summary:	Will print a variable width font to the screen
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "BP_LogoOut.h"

void BP_ShowLogo()
{
	u32 StartTime = Frame_Count;
	//set up BG Modes
	// <summary>	Sets BG0 to use chaacter base block 31 for rendering. </summary>
	BGREG0 = (31 << 8) | (1 << 6) | 1;
	// <summary>	Sets BG0 to use chaacter base block 30 for rendering. </summary>
	BGREG1 = (30 << 8) | (1 << 6) | (1 << 2);

	//Create Map Data
	//=================================
	MapAtt BPLogo;

	BPLogo.ScreenBase = SCREEN_BASE_BLOCK16(31);
	BPLogo.CharBase = CHAR_BASE_BLOCK16(0);
	BPLogo.CurrentX = 0;
	BPLogo.CurrentY = 0;
	BPLogo.MapSrc = (unsigned short*)BP_LogoData;
	BPLogo.MapHeight = 20;
	BPLogo.MapWidth = 30;
	BPLogo.TileOffSet = 0;
	//=================================

	//copy over the pallet for the background
	BP_memcpy16(&LogoPallet, &BG_Pallet[0], 15);
	//copy over the tile set
	BP_memcpy16(&BP_LogoTiles, &BPLogo.CharBase[0], (BP_LogoTileCount * 16));

	BP_ClearText();
	BP_PrintText("Chrismillin.co.uk\0", 11,16);
	BP_FinishWrite();

	BP_RenderMap(BPLogo);
	
	MOSAICAdd = (7 << 4) | 7;

	BP_BlockOut();
	
	bool Skip = false;

	StartTime = Frame_Count;

	while ((Frame_Count < (120 + StartTime)) && (Skip == false))
	{
		BP_UpdateKeys();
		if (BP_isKeyPressed(Key_A))
				Skip = true;
		BP_VBlankWait();
	}

	BP_BlockIn();


}