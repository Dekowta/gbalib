

//Other Lib Includes
//=================================
#include <maxmod.h>		// maxmod library
#include "soundbank.h"		// created by building project
#include "soundbank_bin.h"	// created by building project
//=================================

//Black Paw Includes
//=================================
#include "BP_ToolSet.h"
#include "BP_ToolFunct.h"
#include "BP_Graphics.h"
#include "BP_Irq.h"
#include "BP_Input.h"
#include "BP_NoCash.h"
#include "BP_Writer.h"
#include "BP_Collision.h"
#include "BP_Font.h"
#include "BP_LogoOut.h"
//=================================

//External Includes
//=================================
#include "TestObj.h"

#include "L1.h"
#include "L1Tiles.h"

#include "GM_General.h"
#include "GM_Player.h"
#include "GM_AI.h"
#include "GM_AITemp.h"
#include "GM_Map.h"
#include "GM_BulletC.h"
//=================================


u16 AIAdded[14];

u16 AICount = 0;

//Test Templater for a type 0 AI
GMAITemp AI[] = {
{
	0,
	300,
	40,
	64,
	64,
},
{
	0,
	350,
	40,
	64,
	64,
},
{
	0,
	450,
	40,
	64,
	64,
}, 
{
	1,
	546,
	40,
	64,
	64,
},
{
	0,
	700,
	40,
	64,
	64,
},
{
	1,
	880,
	40,
	64,
	64,
}, 
{
	1,
	1030,
	40,
	64,
	64,
}, 
{
	0,
	1030,
	40,
	64,
	64,
},
{
	0,
	1120,
	40,
	64,
	64,
},
{
	1,
	1320,
	40,
	64,
	64,
},
{
	1,
	1610,
	40,
	64,
	64,
},
{
	1,
	1850,
	40,
	64,
	64,
},
{
	1,
	2030,
	40,
	64,
	64,
}, 
{
	0,
	2030,
	40,
	64,
	64,
},
{
	0,
	2265,
	40,
	64,
	64,
}
};


////////////////////////////////////////////////////////////////////////////////
//                         Global Definitions                                 //
////////////////////////////////////////////////////////////////////////////////

u32 Frame_Count = 0;

//This is the Global Position of the map in pixels and not tiles
Vector2 GlobalMapPos;

Vector2 GlobalPlayerPos;


MapAtt CurrentLvL;


//This is a cheap hack which Should never be used for anything
bool MapFowards = false;
bool MapBack = false;

////////////////////////////////////////////////////////////////////////////////
//                            Prototypes                                      //
////////////////////////////////////////////////////////////////////////////////

void FrameTimer()
{
	//defined in Toolset so I can be used globaly
	Frame_Count++;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Main entry-point for this application. </summary>
///
/// <remarks>	Chris, 10/06/2012. </remarks>
///
/// <returns>	Exit-code for the process - 0 for success, else an error code. </returns>
////////////////////////////////////////////////////////////////////////////////////////////////////

int main(void) {
		// the vblank interrupt must be enabled for VBlankIntrWait() to work since the default
		// dispatcher handles the bios flags no vblank handler is required.
		BP_InitIRQ();

		BP_EnableIRQ(IRQ_Vblank, FrameTimer);

		/// <summary>	Sets The Graphics mode for sprites and OBJs. </summary>
		GFX_DISPCNT = (GFX_MODE0 | GFX_OBJ_1D | GFX_OBJ_On | GFX_BG0_On | GFX_BG1_On);

		//=================================
		WFormat Font;

		Font.ScreenBase 	= SCREEN_BASE_BLOCK16(30);
		Font.CharBase	  	= &CHAR_BASE_BLOCK16(1)[8];
		Font.MaxTiles	  	= 100;
		Font.PalletColours 	= BG_Pallet[16]; //tmp
		Font.FontType 		= &BP_FontData[0];
		//=================================

		//BP_InitaliseOAM(&OAM_Buffer[0], 128);

		BP_InitaliseWriter(Font);

		BP_ShowLogo();

		BP_ClearText();

		/// <summary>	Sets The Graphics mode for sprites and OBJs. </summary>
		GFX_DISPCNT = (GFX_MODE0 | GFX_OBJ_1D | GFX_OBJ_On | GFX_BG0_On | GFX_BG1_On);

		/// <summary>	Sets BG0 to use chaacter base block 31 for rendering. </summary>
		BGREG0 = (31 << 8) | (1 << 6) | 3;
		/// <summary>	Sets BG0 to use chaacter base block 30 for rendering. </summary>
		BGREG1 = (30 << 8) | (1 << 6) | (1 << 2);


		//Create Map Data
		//=================================
		CurrentLvL.ScreenBase = SCREEN_BASE_BLOCK16(31);
		CurrentLvL.CharBase = CHAR_BASE_BLOCK16(0);
		CurrentLvL.CurrentX = 0;
		CurrentLvL.CurrentY = 0;
		CurrentLvL.MapSrc = (unsigned short*)L1Data;
		CurrentLvL.MapHeight = 21;
		CurrentLvL.MapWidth = 20;
		CurrentLvL.TileOffSet = 0;
		//=================================

		//since the map starts on 0,0 then that should be the start 
		GlobalMapPos.x = 8; 
		GlobalMapPos.y = 8;

		//copy over the pallet for the background
		BP_memcpy16(&P0, &BG_Pallet[0], 16);
		BP_memcpy16(&P1, &BG_Pallet[16], 16);

 		//copy over the tile set
 		BP_memcpy16(&L1Tiles, &CurrentLvL.CharBase[0], (L1TileCount * 16));

 		//InitaliseML1(&CurrentMidMap);

		InitaliseML1(&CurrentLvL);

		BP_BlockOut();

		InitalisePlayer(50 - SCREENOFFSET,50 - SCREENOFFSET, 50, 50);

		InitaliseAI();

		// <summary>	Main Loop. </summary>
		while(1)
		{
			//BP_VBlankWait();

			MapFowards = false;
			MapBack = false;
			int i = 0;



			BP_UpdateKeys();

			UpdatePlayer(&CurrentLvL);

			UpdateMap1();
		
			UpdateAI(&CurrentLvL);

			CheckBullets();

			BP_OAMCpy(OAMAddress, OAM_Buffer, 128);


			u16 WorldX = GlobalMapPos.x;

			WorldX += 300;

			for(i = 0; i < 14; i++)
			{
				if (WorldX <= AI[i].MapPosX)
				{
					u16 xx = 0;
					for(xx = 0; xx < 14; xx++)
					{
						if (AIAdded[xx] != i)
						{
							AddAI(Inocent, &AI[i]);
							AIAdded[AICount] = i;
							AICount++;
							break;
						}
					}
				}
			}

			BP_VBlankWait();
		}
}