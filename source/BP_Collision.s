
    .text               
    .align 2           
    .thumb_func        
	.code 16
	.global BP_CIntersectBox		@ I believe this is the return value for the function or to diclare it has one

BP_CIntersectBox:
	push	{r4, lr}

	@aMinX <= bMaxX
	ldrh	r2, [r1]		@bminX
	ldrh	r3, [r1, #4]	@bwidth
	add		r2, r3			@r2 = bminX + bwidth
	ldrh	r3, [r0]		@aminX
	cmp		r3, r2			@
	bgt		.LendF			@aminX > bmaxX  (aminX <= bmaxX if ture)

	@aMaxX >= bMinX
	ldrh	r2, [r0]		@aminX
	ldrh	r3, [r0, #4]	@awidth
	add		r2, r3			@r2 = aminX + awidth
	ldrh	r3, [r1]		@bminX
	cmp		r2, r3			@
	blt		.LendF

	@aMinY <= bMaxY
	mov		r4, #2
	ldrh	r2, [r1, r4]	@bminY
	mov		r4, #6
	ldrh	r3, [r1, r4]	@bheight
	add		r2, r3			@r2 = bminY + bheight
	mov		r4, #2
	ldrh	r3, [r0, r4]	@aminY
	cmp		r3, r2			@
	bgt		.LendF			@aminY > bmaxY  (aminY <= bmaxY if ture)

	@aMaxY >= bMinY
	mov		r4, #2
	ldrh	r2, [r0, r4]	@aminY
	mov		r4, #6
	ldrh	r3, [r0, r4]	@aheight
	add		r2, r3			@r2 = aminY + aheight
	mov		r4, #2
	ldrh	r3, [r1, r4]	@bminY
	cmp		r2, r3			@
	blt		.LendF
	
	@True
	pop		{r4}
	pop		{r1}
	mov		r0, #1
	bx		r1

	@False
.LendF:
	pop		{r4}
	pop		{r1}
	mov		r0, #0
	bx		r1

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


    .text               
    .align 2            
    .thumb_func         
	.code 16
	.global BP_CIntersectCircle

BP_CIntersectCircle:
	push	{r4-r7, lr}

	@dx = x2 - x1
	mov		r7, #0
	ldrsh	r2, [r0, r7]		@loads x from circle a
	ldrsh	r3, [r1, r7]		@loads x from circle b
	sub		r4, r3, r2

	@dy = y2 - y1
	mov		r7, #2
	ldrsh	r2, [r0, r7]	@loads y from circle a
	ldrsh	r3, [r1, r7]	@loads y from circle b
	sub		r5, r3, r2

	@dr = rd2 + rd1
	mov		r7, #4
	ldrsh	r2, [r0, r7]	@loads radius from circle a
	ldrsh	r3, [r1, r7]	@loads radius from circle b
	add		r6, r3, r2

	@dx * dx + dy * dy = rs
	mov		r7, r4
	mul		r4, r4, r7		@dx * dx = rs1
	mov		r7, r5
	mul		r5, r5, r7		@dy * dy = rs2
	add		r2, r4, r5			@rs1 + rs2

	@rs < (dr * dr)
	mov		r7, r6
	mul		r6, r6, r7
	cmp		r2, r6
	bgt		.Lend

	pop		{r4-r7}
	pop		{r1}
	mov		r0, #1
	bx		r1

.Lend:
	pop		{r4-r7}
	pop		{r1}
	mov		r0, #0
	bx		r1

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


	@The first step is to work out how many tiles we need to check and the offset
	@this also needs to work with any collision box size!

	@the way this works is to check the width and height and the x and y
	@if the (width + x) or (height + y) is divisable by 8 then the amount needed to be
	@checked is reduced by 1
	@
	@so an example if the collision box is on 37, 9 and is 15, 17 then the first step is
	@to divide the x and y by 8
	@
	@37/8 = 4 r 5 = x
	@ 9/8 = 1 r 1 = y
	@
	@the next step is add the x and y to the width and height
	@
	@
	@37 + 15 = 52
	@9  + 17 = 26
	@
	@now divide these by 8
	@
	@52/8 = 6 r 4
	@26/8 = 3 r 2
	@
	@NOTE: SAVE THE REMAINDER HERE
	@
	@6 - 4 = 2
	@3 - 1 = 2
	@
	@so according to this we are only going to check 2
	@tiles and also 2 from the x and y 
	@this is incorrect and it should be 3, 3
	@but we knew this was incorrect as the 52/8 and 26/8 produced a remainder
	@so we +1 to both the width and height values

	@another example 32,8 and 15, 17
	@
	@32/8 = 4 = x
	@ 8/8 = 1 = y
	@
	@32 + 15 = 47
	@8  + 17 = 25
	@
	@47/8 =  5 r 7
	@25/8 =  3 r 1
	@
	@5 - 4 = 1
	@3 - 1 = 2
	@
	@since we had remainders its
	@
	@2 and 3 this time which is correct

	@Another example however this time the width and height
	@x y will land on a value that does not produce a remainder
	@
	@32, 8 | 16, 16
	@
	@32/8 = 4 = x
	@ 8/8 = 1 = y
	@
	@32 + 16 = 48
	@ 8 + 16 = 24
	@
	@48/8 = 6
	@24/8 = 3
	@
	@6 - 4 = 2
	@3 - 1 = 2
	@
	@no remainder no need to add values


	@with the map data its a little broken since the y and x are switched
	@the correct method to get a position is [(x * height) + y]
	@
	@this is due to the way the array is laid out in memory. there are width amount
	@of rows and each row contains height amount of columns



    .text               @ Put the following stuff in the "text" (== code) section
    .align 2            @ align to 1<<2 bytes
    .thumb_func         @ Call as thumb function
	.code 16
	.global BP_CMapBox
	.global CollisionPos

BP_CMapBox:
	push	{r4-r7, lr}
	push	{r0}

	@r0 = free
	@r1 = collison box
	@r2 = collision x
	@r3 = collision width
	@r4 = free
	@r5 = remainder from width calcualtion
	@r6 = final width check
	@r7 = Free


	ldrh	r2, [r1]			@obtain collision x
	mov		r4, #4
	ldrh	r3, [r1, r4]		@obtain the collision width
	add		r7,	r2, r3			@x plus width
	lsr		r4, r7, #3			@(x + width)/8
	lsl		r0, r4, #3			@r0 = r4 * 8
	sub		r5, r7, r0			@r5 = r7 - r0
	lsr		r7, r2, #3			@x/8
	sub		r6, r4, r7			@(x + width)/8 - x/8
	cmp		r5, #0				@is there a remainder for the width
	beq		.Lj1
	add		r6, #1				@add 1 is there was


	@r0 = free
	@r1 = collison box
	@r2 = collision y
	@r3 = collision height
	@r4 = free
	@r5 = final width check
	@r6 = final height check
	@r7 = Free

.Lj1:
	
	push	{r6}				@save the width check value

	mov		r4, #2
	ldrh	r2, [r1, r4]		@obtain collision y
	mov		r4, #6
	ldrh	r3, [r1, r4]		@obtain the collision height
	add		r7,	r2, r3			@y plus height
	lsr		r4, r7, #3			@(y + height)/8
	lsl		r0, r4, #3			@r0 = r4 * 8
	sub		r5, r7, r0			@r5 = r7 - r0
	lsr		r7, r2, #3			@y/8
	sub		r6, r4, r7			@(y + height)/8 - x/8
	cmp		r5, #0				@is there a remainder for the width
	beq		.Lj2
	add		r6, #1				@add 1 is there was

.Lj2:

	pop		{r5}
	pop		{r0}


	@r0 = mapatt	(free once pushed)
	@r1 = collison box (map height once pushed)
	@r2 = x
	@r3 = y
	@r4 = free
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address
	
	@load x and y
	ldrh	r2, [r1]		@x collision
	mov		r4, #2
	ldrh	r3, [r1, r4]	@y collision
	push	{r1}			@collision box stored
		
	@load map width and address
	mov		r4, #8
	ldr		r7, [r0, r4]	@map data
	mov		r4, #14
	ldrh	r1, [r0, r4]	@map height

	@move the map address to the correct location
	lsr		r2, #3			@x/8
	lsr		r3, #3			@y/8
	push	{r2-r3}			@temp save the x and y
	sub		r3, #1			@y - 1 
	mul		r1, r1, r2		@x * height
	lsl		r1, #1			@to add to the address its a step of 2 rather than 1 so everything has to be					
	lsl		r3,	#1			@multiplied by 2
	add		r7, r7, r1		@add + (x*height)
	add		r7, r7, r3		@add + ((x*height) + (y-1))

	@recover  the map quickly to get the height	
	mov		r4, #14
	ldrh	r1, [r0, r4]	@map height
	lsl		r1, #1			@map height * 2

	@r0 = mapatt
	@r1 = (1 * height) * 2
	@r2 = free
	@r3 = free
	@r4 = counter
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

	@now to loop through the array to get the data
	mov		r4, #0
.LloopTop:
	ldrh	r2, [r7]		@load the src tile data
	mov		r3, #3			@r3 = 3
	lsl		r3, #14			@(r3 << 14)
	and		r3, r2			@r3 & data
	lsr		r3, #14			@r3 >> 14
	cmp		r3, #1			@r3 == 1 (1 is a collision object)
	beq		.LTop1
	add		r7, r1
	add		r4, #1	
	cmp		r4, r5
	bne		.LloopTop	


	@r0 = mapatt
	@r1 = height * 2
	@r2 = free
	@r3 = y offset
	@r4 = free
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

.Lj3:
	@now need to check the bottom for collision
	pop		{r2, r3}		@get x and y back
	push	{r2, r3}		@save them again
	add		r3, r6			@y + tiles to check height
	lsr		r1, #1			@map height /2 (was already * 2 any way		
	mul		r2, r2, r1		@(x*height)
	lsl		r1, #1			@(1*height) * 2
	lsl		r2, #1			@(x*height) * 2
	lsl		r3, #1			@(y + collisionheight) * 2
	mov		r4, #8
	ldr		r7, [r0, r4]	@map data
	add		r7, r7, r2		@add + (x*height)
	add		r7, r7, r3		@add + ((x*height) + (y-1))

	@r0 = mapatt
	@r1 = (1 * height) * 2
	@r2 = free
	@r3 = free
	@r4 = counter
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

	@now to loop through the array to get the data
	mov		r4, #0
.LloopBottom:
	ldrh	r2, [r7]		@load the src tile data
	mov		r3, #3			@r3 = 3
	lsl		r3, #14			@(r3 << 14)
	and		r3, r2			@r3 & data
	lsr		r3, #14			@r3 >> 14
	cmp		r3, #1			@r3 == 1 (1 is a collision object)
	beq		.LBottom1
	add		r7, r1
	add		r4, #1	
	cmp		r4, r5
	bne		.LloopBottom	


	@r0 = mapatt
	@r1 = height * 2
	@r2 = free
	@r3 = free
	@r4 = free
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

.Lj4:
	@Time to check the left
	pop		{r2, r3}		@get x and y back
	push	{r2, r3}		@save them again
	sub		r2, #1			@x - 1
	lsr		r1, #1			@map height /2 (was already * 2 any way		
	mul		r2, r2, r1		@(x*height)
	lsl		r1, #1			@(1*height) * 2
	lsl		r2, #1			@(x*height) * 2
	lsl		r3, #1			@y * 2
	mov		r4, #8
	ldr		r7, [r0, r4]	@map data
	add		r7, r7, r2		@add + (x*height)
	add		r7, r7, r3		@add + ((x*height) + (y-1))

	@r0 = mapatt
	@r1 = height * 2
	@r2 = free
	@r3 = free
	@r4 = counter
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

	mov		r4, #0
.LloopLeft:
	lsl		r3, r4, #1		@y + i (since each jump is a 16 bit value it needs to be * 2
	ldrh	r2, [r7, r3]	@load the data
	mov		r3, #3			@r3 = 3
	lsl		r3, #14			@(r3 << 14)
	and		r3, r2			@r3 & data
	lsr		r3, #14			@r3 >> 14
	cmp		r3, #1			@r3 == 1 (1 is a collision object)
	beq		.LLeft
	add		r4, #1	
	cmp		r4, r6
	bne		.LloopLeft	

	@r0 = mapatt
	@r1 = height * 2
	@r2 = free
	@r3 = free
	@r4 = free
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

.Lj5:
	@Time to check the right
	pop		{r2, r3}		@get x and y back
	push	{r2, r3}		@save them again
	add		r2, r5			@x + final width
	lsr		r1, #1			@map height /2 (was already * 2 any way		
	mul		r2, r2, r1		@(x*height)
	lsl		r1, #1			@(1*height) * 2
	lsl		r2, #1			@(x*height) * 2
	lsl		r3, #1			@y * 2
	mov		r4, #8
	ldr		r7, [r0, r4]	@map data
	add		r7, r7, r2		@add + (x*height)
	add		r7, r7, r3		@add + ((x*height) + (y-1))

	@r0 = mapatt
	@r1 = height * 2
	@r2 = free
	@r3 = free
	@r4 = free
	@r5 = final width check
	@r6 = final height check
	@r7 = MapData address

	mov		r4, #0
.LloopRight:
	lsl		r3, r4, #1		@y + i (since each jump is a 16 bit value it needs to be * 2
	ldrh	r2, [r7, r3]	@load the data
	mov		r3, #3			@r3 = 3
	lsl		r3, #14			@(r3 << 14)
	and		r3, r2			@r3 & data
	lsr		r3, #14			@r3 >> 14
	cmp		r3, #1			@r3 == 1 (1 is a collision object)
	beq		.LRight
	add		r4, #1	
	cmp		r4, r6
	bne		.LloopRight
	
	b		.Lj6



.LLeft:
	pop		{r2, r3}		@recover x and y
	push	{r2, r3}		@save x and y

	sub		r2, r2, #1		@x - 1
	ldr		r3,=CollisionPos@get the collisionPos struct address
	mov		r4, #8
	strh	r2, [r3, r4]	@save the x pos
	mov		r4, #10			@
	mov		r2, #1
	strh	r2,	[r3, r4]	@set the left as triggered
	b		.Lj5

.LRight:
	pop		{r2, r3}		@recover x and y
	push	{r2, r3}		@save x and y

	add		r2, r5			@x + width to check
	ldr		r3,=CollisionPos@get the collisionPos struct address
	mov		r4, #12
	strh	r2, [r3, r4]	@save the x pos
	mov		r4, #14			@
	mov		r2, #1
	strh	r2,	[r3, r4]	@set the left as triggered
	b		.Lj6

.LTop1:
	pop		{r2, r3}		@recover x and y
	push	{r2, r3}		@save x and y

	sub		r3, r3, #1		@y - 1
	ldr		r2,=CollisionPos@get the collisionPos struct address
	mov		r4, #4			@
	strh	r3, [r2, r4]	@save the y pos
	mov		r4, #6			@
	mov		r3, #1
	strh	r3,	[r2, r4]	@set the top as triggered
	b		.Lj3

.LBottom1:
	pop		{r2, r3}		@recover x and y
	push	{r2, r3}		@save x and y

	add		r3, r6			@y + height to check
	ldr		r2,=CollisionPos@get the collisionPos struct address
	strh	r3, [r2]		@save the y pos
	mov		r4, #2			@
	mov		r3, #1
	strh	r3,	[r2, r4]	@set the bottom as triggered
	b		.Lj4





.Lj6:
	@If all the above works then you have found the collision objects
	@but hold up we are an 8th smaller atm so need to scale everything up
	@and check to see if we did actually collide
	pop		{r2, r3}
	pop		{r1}

	@r0 = mapatt
	@r1 = CollisionBox
	@r2 = x/8	(unneeded now any way) 
	@r3 = y/8	(unneeded now)
	@r4 = free
	@r5 = free
	@r6 = free
	@r7 = free

	ldr		r7,=CollisionPos

	mov		r4, #2
	ldrh	r5, [r1, r4]	@obtain y  Fixed
	mov		r4, #2
	ldrh	r0, [r7, r4]	@load the bool for the bottom
	cmp		r0, #1			@check to see if it was triggered
	bne		.Lj7
	ldrh	r0, [r7]		@load where the collision was triggered
	lsl		r0, #3			@position * 8 (to upscale it)
	mov		r4, #6
	ldrh	r6, [r1, r4]	@load the height of the collision object
	sub		r0, r0, r6		@position * 8 - height 
	cmp		r5, r0
	bge		.Lj7
	mov		r6, #0
	mov		r4, #2
	strh	r6, [r7, r4]	@set bottom trigger to false


.Lj7:
	mov		r4, #2
	ldrh	r5, [r1, r4]	@obtain y
	mov		r4, #6
	ldrh	r0, [r7, r4]	@load the bool for the bottom
	cmp		r0, #1			@check to see if it was triggered
	bne		.Lj8
	mov		r4, #4
	ldrh	r0, [r7, r4]		@load where the collision was triggered
	lsl		r0, #3			@position * 8 (to upscale it)
	add		r0, #8			@(position * 8) + 8
	cmp		r5, r0
	ble		.Lj8
	mov		r6, #0
	mov		r4, #6
	strh	r6, [r7, r4]	@set bottom trigger to false
	
.Lj8:
	ldrh	r5, [r1]		@obtain x
	mov		r4, #10
	ldrh	r0, [r7, r4]	@load the bool for the left
	cmp		r0, #1			@check to see if it was triggered
	bne		.Lj9
	mov		r4, #8
	ldrh	r0, [r7, r4]	@load where the collision was triggered
	lsl		r0, #3			@position * 8 (to upscale it)
	add		r0, #8			@position * 8 - height 
	cmp		r5, r0
	ble		.Lj9
	mov		r6, #0
	mov		r4, #10
	strh	r6, [r7, r4]	@set bottom trigger to false

.Lj9:
	ldrh	r5, [r1]		@obtain x
	mov		r4, #14
	ldrh	r0, [r7, r4]	@load the bool for the right
	cmp		r0, #1			@check to see if it was triggered
	bne		.Lj10
	mov		r4, #12
	ldrh	r0, [r7, r4]	@load where the collision was triggered
	lsl		r0, #3			@position * 8 (to upscale it)
	mov		r4, #4
	ldrh	r6, [r1, r4]	@load the width of the collision object
	sub		r0, r0, r6		@position * 8 - width
	cmp		r5, r0
	bge		.Lj10
	mov		r6, #0
	mov		r4, #14
	strh	r6, [r7, r4]	@set bottom trigger to false

.Lj10:
	@bottom
	mov		r4, #2
	ldrh	r1, [r7, r4]
	cmp		r1, #1
	beq		.LEndT

	@top
	mov		r4, #6
	ldrh	r1, [r7, r4]
	cmp		r1, #1
	beq		.LEndT

	@left
	mov		r4, #10
	ldrh	r1, [r7, r4]
	cmp		r1, #1
	beq		.LEndT

	@right
	mov		r4, #14
	ldrh	r1, [r7, r4]
	cmp		r1, #1
	beq		.LEndT
	b		.LEnd


.LEnd:
	pop		{r4-r7}
	pop		{r1}
	mov		r0, #0
	bx		r1

.LEndT:
	pop		{r4-r7}
	pop		{r1}
	mov		r0, #1
	bx		r1	
