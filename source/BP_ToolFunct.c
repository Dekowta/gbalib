////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Text Writer
//
// summary:	Will print a variable width font to the screen
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "BP_ToolFunct.h"

void BP_memcpy8(unsigned char* src, unsigned char* dst, unsigned short count)
{
	//while count != 0 count = count - 1
	while(count--)
		*dst++ = *src++;
	// this is the best method of copying memory from one destination to another
	// it de-refs both items and copys the data from one location to the other
	// after the address is incremented by 1 (in this case 8bits)

}

void BP_memcpy16(unsigned short* src, unsigned short* dst, unsigned short count)
{
	while(count--)
		*dst++ = *src++;
}

void BP_memcpy32(unsigned int* src, unsigned int* dst, unsigned short count)
{
	while(count--)
		*dst++ = *src++;
}


//stores the return for the BP_itoa function
//as I believe the malloc call will keep allocating memory
//every time it is called. and putting output[33] into the function
//will clear the data once the function has exited so the pointer
//will be pointing to nothing

//The danger of doing it this was is that Output can only ever contain 1 value at a time
//which means that you cant store the data returned from itoa and you must move it across
//to a new string or print the data then and there.
char output[33];


char asciilut[] = 
{
	48,
	49,
	50,
	51,
	52,
	53,
	54,
	55,
	56,
	57,
	65,
	66,
	67,
	68,
	69,
	70,
};


char* BP_itoa(unsigned int src, enum IntFormats type)
{
	int i = 0;
	int ii = 0;
// 	char* output;
// 	output = (char*)malloc(33);

	switch (type)
	{
	case BINARY: //binary
		{
			int ii = 0;
			for (i = 31; i >= 0; i--)
				output[ii++] = asciilut[((src >> i) & 1)];
			output[32] = '\0';
			break;
		}
	case OCTAL: //octal
		{
			ii = 0;
			for (i = 10; i >= 0; i--)
				output[ii++] = asciilut[((src >> (i * 3)) & 7)];
			output[11] = '\0';
			break;
		}
	case BASE10L: //base 10 leading 0
		{
			unsigned int Number = src;
			ii = 9;
			for (i = 0; i < 10; i++)
			{
				output[ii--] = asciilut[Number % 10];
				Number /= 10;
			}
			output[10] = '\0';
			break;
		}
	case BASE10: //base 10
		{
			unsigned int Number = src;
			unsigned char LastDigit = 0;
			unsigned short Numbers[10];
			output[0] = '0';
			for (i = 0; i < 10; i++)
			{
				unsigned short Num = Number % 10;
				if (Num != 0)
					LastDigit = i;
				Numbers[i] = Num;
				Number /= 10;
			}
 			ii = 0;
			for (i = LastDigit; i >= 0; i--)
				output[ii++] = asciilut[Numbers[i]];
			output[++LastDigit] = '\0';
			break;
		}
	case HEX: //hex
		{
			output[0] = 48;
			output[1] = 120;
			ii = 2;
			for (i = 7; i >= 0; i--)
				output[ii++] = asciilut[((src >> (i * 4)) & 15)];
			output[10] = '\0';
			break;			
		}
	default:
		{
			unsigned int Number = src;
			ii = 9;
			for (i = 0; i < 10; i++)
			{
				output[ii--] = asciilut[Number % 10];
				Number /= 10;
			}
			output[10] = '\0';
			break;
		}
	}
	return &output;
}