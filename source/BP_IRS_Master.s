
@This will manage IRQ  service routines. It also deals with
@software Interrupts as well This is an adaptation of Tonc's
@Master ISR (well just commented better)
@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	.file "BP_IRS_Master.s"
	.extern IRS_Table			@Includes the user created IRS_Table

	.section .iwram, "ax", %progbits
    .arm
    .align
    .global IRS_Master

IRS_Master:                @ Label for function entry point.
    @ start of function
	
	@reads in the IF and IE and ands them together
	mov		r0, #0x04000000		@Moves the address of Reg Base into r0
	ldr		ip, [r0, #0x200]!	@load a register Reg Base + #0x200 which is IRQ_IE
	and		r2, ip, ip, lsr #16 @Since we are in arm mode i believe ip is 32 bit and is storing IE in the low bit
								@and IF in the high bit so the lsr moves the IF now to the same position as the IE
								@and they then get AND together

	@Check the IRQ in the IF and BIOS IF
	strh	r2, [r0, #2]		@ok so this seems to work in the opersite way to all the other
								@opperands (UHHHH) I believe we are moving a half word into r0 shifted 2
								@now I would expect this to be 0x04000002 but i think it is 0x04000202
								@as the ! in the ldr may have also applied the 0x200 to the 0x04000000
								@the above is correct r0 is 0x04000200 because of the !		

	ldr		r3, [r0, #-0x208]	@Now this should move into IWRAM and get the BIOS IF
								@If you look at the GBAspec for no$cash and look for
								@the BIOS IF you will notice it is not 0x3FFFFFA (0x400202 - 0x208)
								@and is infact 0x03007FF8. This is due to the GBA mirroring
								@0x03007FXX to 03FFFFXX
	orr		r3, r3, r2			@Now the IE&IF are orred with the BIOS IF
	str		r3, [r0, #-0x208]	@and now move the BIOS IF back into the BIOS IF register

	@Time to search for the irq that has been triggered
	ldr		r1, =IRS_Table

	@since we have not picked any lable to jump to it will continue into irq_search
.Lirq_search:
	ldr		r3, [r1], #8		@r3 is going to store the irq flag from are table
								@and every loop it will move the register r1 (our IRS Table)
								@by 8(but I guess that is the size of a void pointer)
								@however that is a little bit iffy as it would imply a 64bit
								@system as void should be 2 for 16bit 4 for 32 and 8 for 64
	tst		r3, r2				@this will AND r3 and r2 together and set the N and Z flags based
								@on the result but will discard the result
	bne		.Lpost_search		@If a flag is found then it will trigger to be a 1 and not equal to 0
	cmp		r3, #0
	bne		.Lirq_search		@will loop back round if none are found
	
.Lpost_search:
	ldrne	r1,	[r1, #-4]		@Since the way the loop has worked is that it does the ++
								@before the test is perfromed we need to go back one step
								@to check the irs since we have moved 1 extra space
	cmpne	r1, #0
	beq		.Lexit				@return if there is no irs to perform

	@if there is an irs we need to execute it
	ldr		r3, [r0, #8]		@get the IME address which is 8 off from 0x04000200
	strb	r0, [r0, #8]		@set IME to 0
	bic		r2, ip, r2			@Inverts the IE&IF and then ANDs it
	strh	r2,	[r0]			@clears the IE
	
	mrs		r2, spsr			@copies the SPSR into r2
	stmfd	sp!, {r2-r3, ip, lr}@stores the sprs, IME, IE&IF, lr_irq onto the stack
	
	@set the mode back into user so we can execture the irs as normal
	mrs		r3, cpsr			@copies the current program status register into r3
								@this holds information about the current program such
								@as the current program mod. This is something we want to change
	bic		r3, r3, #0xDF		@!unsure of what this is but im guessing it sets up the user mode
								@by changing the memory for the current program mode
	orr		r3, r3, #0x1F		
	msr		cpsr, r3			@This i believe will set the current program status
								@so will kick it into user mode now
	
	@now we are in user mode we can start running our isr
	stmfd	sp!, {r0,lr}		@ save the registers to the stack before running
	mov		lr, pc
	bx		r1					@breach into r1 and the x means it will perfrom it in thumb mode
								@which is good since the function should be compiled in thumb any way
	ldmfd	sp!, {r0,lr}		@retrun the registers stored on the stack
	
	@clear the IME again to make sure its all clear before carring on
	strb	r0, [r0, #8]
	
	@reset the cpu into irq mode
	mrs		r3, cpsr
	bic		r3, r3, #0xDF
	orr		r3, r3, #0x92
	msr		cpsr, r3
	
	@restore the stack we saved a while ago
	ldmfd	sp!, {r2-r3, ip, lr}
	msr		spsr, r2			@restore the spsr
	strh	ip, [r0]			@restore the IE
	str		r3, [r0, #8]		@restore the IME

.Lexit:	
	bx		lr	


    @ end of function 