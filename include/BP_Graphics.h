////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Black Paw Graphic functions
//
// summary:	Declares the bp graphics class
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BP_GRAPHICS_H
#define BP_GRAPHICS_H

#include "BP_ToolSet.h"
#include "BP_ToolFunct.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	define functions for creating OAM atts. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

#define Set_ATTR0(y, enable, shape)			(((y) & 255) | (((enable) & 1) << 9) |(((shape) & 3) << 14))
#define Set_ATTR1(x, Vflip, Hflip, size)	(((x) & 511) | (((Hflip) & 1) << 12) | (((Vflip) & 1) << 13) |(((size) & 3) << 14))
#define Set_ATTR2(ID, Priority, Pallet)		(((ID) & 1023) | (((Priority) & 3) << 10) |(((Pallet) & 15) << 12))

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	OAM Bit Count. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//There are a few Masks and positions that are important for OAM so its quick to
//manipulate the bit pattern
//
// The OAM Masks and positions are as followed


//enum with all the masks and shifts for the OAM
typedef enum OAMAttMS
{
	//The y position mask and shift
	//0-7bits are used for this shift so 
	//0x07c is used for the mask and since
	//the y pos is at the start the shift is 0 
	OAMATT0_Y_Mask		 = 0x0ff,
	OAMATT0_Y_Shift		 = 0x000,
	OAMATT0_Enable_Mask	 = 0x001,
	OAMATT0_Enable_Shift = 0x009,
	OAMATT0_Shape_Mask	 = 0x003,
	OAMATT0_Shape_Shift	 = 0x00d,


	OAMATT1_X_Mask		= 0x1ff,
	OAMATT1_X_Shift		= 0x000,
	OAMATT1_VFlip_Mask	= 0x001,
	OAMATT1_VFlip_Shift	= 0x00c,
	OAMATT1_HFlip_Mask	= 0x001,
	OAMATT1_HFlip_Shift	= 0x00b,
	OAMATT1_Size_Mask	= 0x003,
	OAMATT1_Size_Shift	= 0x00d,


	OAMATT2_TID_Mask	   = 0x3ff,
	OAMATT2_TID_Shift	   = 0x000,
	OAMATT2_Priority_Mask  = 0x003,
	OAMATT2_Priority_Shift = 0x009,
	OAMATT2_Pallet_Mask	   = 0x00f,
	OAMATT2_Pallet_Shift   = 0x00b

}OAMAttMS;

#define OAMATT0_HIDE 0x0200

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	OAM Presets. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum OAMPresets
{
	//Att0 Presets
	OAMATT0_Shape_Square =  0,
	OAMATT0_Shape_Wide	 = (1 << 14),
	OAMATT0_Shape_Tall	 = (2 << 14),

	//Att1 Presets
	OAMATT1_Size_8	= 0,
	OAMATT1_Size_16	= (1 << 14),
	OAMATT1_Size_32	= (2 << 14),
	OAMATT1_Size_64	= (3 << 14)



}OAMPresets;


////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory copy for the OBJ address 4byte (32bit). </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//memcpy32 could be used for this function but there is this function to make it
//a little easier to manage.
void BP_OBJmemcpy32(unsigned int* src, unsigned short count, unsigned short OBJAdd);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory copy for the OBJ address 2byte (16bit). </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//memcpy16 could be used for this function but there is this function to make it
//a little easier to manage.
void BP_OBJmemcpy16(unsigned short* src, unsigned short count, unsigned short OBJAdd);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Initalising OAM/OAM's. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//This will initalise one or more OAM Address to be 0 and not shown so its ready for use later
void BP_InitaliseOAM(SpriteAtt* src, unsigned short count);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Copies over OAM/OAMs to the OAM Address. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_OAMCpy(SpriteAtt* dst, SpriteAtt* src, unsigned short count);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	SetPos Function. This will only effect the position of the sprite. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_SetOAMPos(SpriteAtt* src, unsigned char y, unsigned short x);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Enable of disable a OAM. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_EnableOAM(SpriteAtt* src, bool Enable);


////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	flip a sprite verticaly. </summary>
///
/// <remarks>	Chris, 11/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_OAMFlipV(SpriteAtt* src, bool Flip);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	flip a sprite horizontaly. </summary>
///
/// <remarks>	Chris, 11/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_OAMFlipH(SpriteAtt* src, bool Flip);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Set Tile and pallet of sprite. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_SetSprite(SpriteAtt* src, unsigned short Tile, unsigned short Pallet);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Create an OAM Sprite with SetAttributes. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_SetOAM (SpriteAtt* src, unsigned short Att0, unsigned short Att1, unsigned short Att2);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	BG Tile Drawing is going to use an odd method to
/// 			try and prevent the screen from drawing the tiles
/// 			as they render (gives an odd effect) for this I am going
/// 			to try a odd form of double buffering.
/// 			By this I will render the map to 2 screen blocks 1 which will be
/// 			in use and another which will be used for the "backbuffer". The
/// 			Tiles will be rendered 1 by 1 on the backbuffer screen block and
/// 			then once it is done all that memory will be copied over
/// 			to the frontbuffer.
/// 			This could possibly prevent any oddities when drawing to the screen
/// 			however there are no 2 draw events so it could possibly be slow </summary>
///
/// <remarks>	Chris, 03/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

#define Set_BG(Tile, VFlip, HFlip, Pallet)				(((Tile) & 1023) | ((HFlip)<<10) | ((VFlip)<<11) | ((Pallet)<<12))


typedef struct MapAtt
{
	unsigned short*	CharBase;			//The character base used
	unsigned short*	ScreenBase;			//The Front Screen Base - to be displayed
	unsigned short*	MapSrc;				//A pointer to the map data
	unsigned short	MapWidth;			//the total map width
	unsigned short	MapHeight;			//the total map height
	unsigned short	CurrentX;			//The current X position of the map (related to the map array)
	unsigned short	CurrentY;			//The current Y position of the map
	unsigned short	TileOffSet;
} ALIGN(4) MapAtt;

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	BG Enums. </summary>
///
/// <remarks>	Chris, 03/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum BGPresets
{
	BG_Priority0 = 0,
	BG_Priority1 = 1,
	BG_Priority2 = 2,
	BG_Priority3 = 3,

	BG_Mosaic = (1 << 6),

	BG_256x256 = 0,
	BG_512x256 = (1 << 14),
	BG_256x512 = (2 << 14),
	BG_512x512 = (3 << 14)
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Renders the tiles to the back buffer. </summary>
///
/// <remarks>	Chris, 03/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_RenderScreenBase(unsigned short* src, unsigned short x, unsigned short y, unsigned short Tile, unsigned short Pallet, unsigned short Vflip, unsigned short Hflip);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Renders a map without the backbuffer format. </summary>
///
/// <remarks>	Chris, 03/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_RenderMap(MapAtt MapToRender);

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Mosaics out. </summary>
//
// <remarks>	Chris, 03/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_BlockOut();

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Mosaics in. </summary>
//
// <remarks>	Chris, 03/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_BlockIn();

#endif