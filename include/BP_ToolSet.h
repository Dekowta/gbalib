////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Tool Set
//
// summary:	This is the tool Set for memory addresses and magic numbers for the GBA
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BP_TOOLSET_H
#define BP_TOOLSET_H

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Data Type Defenitions. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//These have been taken from libgba

typedef	unsigned char			u8;		/**< Unsigned 8 bit value	*/
typedef	unsigned short int		u16;	/**< Unsigned 16 bit value	*/
typedef	unsigned int			u32;	/**< Unsigned 32 bit value	*/
typedef	signed char				s8;		/**< Signed 8 bit value	*/
typedef	signed short int		s16;	/**< Signed 16 bit value	*/
typedef	signed int				s32;	/**< Signed 32 bit value	*/
typedef	volatile u8				vu8;	/**< volatile Unsigned 8 bit value	*/
typedef	volatile u16			vu16;	/**< volatile Unigned 16 bit value	*/
typedef	volatile u32			vu32;	/**< volatile Unsigned 32 bit value	*/
typedef	volatile s8				vs8;	/**< volatile Signed 8 bit value	*/
typedef	volatile s16			vs16;	/**< volatile Signed 8 bit value	*/
typedef	volatile s32			vs32;	/**< volatile Signed 8 bit value	*/

typedef enum { false, true } bool;

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory Aligning macros. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

#define ALIGN(n)    __attribute__((aligned(n)))
#define PACKED      __attribute__((packed))

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory Addresses. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

#define EWRAM_DATA __attribute__((section(".ewram")))
#define IWRAM_DATA __attribute__((section(".iwram")))
#define EWRAM_BSS __attribute__((section(".sbss")))

#define EWRAM_CODE __attribute__((section(".ewram"), long_call))
#define IWRAM_CODE __attribute__((section(".iwram"), long_call))

//Start of video memory Address 92kb
#define VRAM		0x06000000
#define VRAMAdd		((unsigned short*) 0x06000000)
//Start of Internal working RAM 32kb
#define IWRAM		0x03000000
#define IWRAMAdd	((unsigned short*) 0x03000000)
//Stat of External working RAM
#define EWRAM		0x02000000 
#define EWRAMAdd	((unsigned short*) 0x02000000)
//Start of save data memory
#define SRAM		0x0E000000 
#define SRAMAdd		((unsigned char*) 0x0E000000) //lane is only 8 bits wide so only char can be used

//a base register location for i/0 
#define REG_BASE 0x04000000

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	LCD/Graphic Addresses. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//------------Screen IRQ & Status------------

//Used to set V/H-Blank Interrupts and seeing there status
#define GFX_DISPSTAT *((unsigned short*)0x04000004)

//=======================================================
//summery from no$spec
//  Bit   Expl.
//  0     V-Blank flag   (Read only) (1=VBlank) (set in line 160..226; not 227)
//  1     H-Blank flag   (Read only) (1=HBlank) (toggled in all lines, 0..227)
//  2     V-Counter flag (Read only) (1=Match)  (set in selected line)
//  3     V-Blank IRQ Enable         (1=Enable)
//  4     H-Blank IRQ Enable         (1=Enable)
//  5     V-Counter IRQ Enable       (1=Enable)
//  6-7   Not used
//  8-15  V-Count Setting (LYC)      (0..227)
//=======================================================

// Will count the vertical scanlines
#define GFX_VCOUNT *((unsigned short*))0x04000006)

//=======================================================
//summery from no$spec
// Indicates the currently drawn scanline, values in range from 160..227 indicate 'hidden' scanlines within VBlank area.
//
//  Bit   Expl.
//  0-7   Current scanline (LY)      (0..227)
//  8-15  Not Used
//=======================================================

//-------------------------------





//------------Screen Setup------------

//sets up the display mode
#define GFX_DISPCNT *((unsigned short*)0x04000000)

//=======================================================
//summery from no$spec
//Bit   Expl.
//0-2   BG Mode                (0-5=Video Mode 0-5, 6-7=Prohibited)
//3     Reserved / CGB Mode    (0=GBA, 1=CGB; can be set only by BIOS opcodes)
//4     Display Frame Select   (0-1=Frame 0-1) (for BG Modes 4,5 only)
//5     H-Blank Interval Free  (1=Allow access to OAM during H-Blank)
//6     OBJ Character VRAM Mapping (0=Two dimensional, 1=One dimensional)
//7     Forced Blank           (1=Allow FAST access to VRAM,Palette,OAM)
//8     Screen Display BG0  (0=Off, 1=On)
//9     Screen Display BG1  (0=Off, 1=On)
//10    Screen Display BG2  (0=Off, 1=On)
//11    Screen Display BG3  (0=Off, 1=On)
//12    Screen Display OBJ  (0=Off, 1=On)
//13    Window 0 Display Flag   (0=Off, 1=On)
//14    Window 1 Display Flag   (0=Off, 1=On)
//15    OBJ Window Display Flag (0=Off, 1=On)
//=======================================================

typedef enum LCDPresets
{
	//Mode
	GFX_MODE0 = 0,
	GFX_MODE1 = 1,
	GFX_MODE2 = 2,
	GFX_MODE3 = 3,
	GFX_MODE4 = 4,
	GFX_MODE5 = 5,

	//BGS
	GFX_BG0_On = (1 << 8),
	GFX_BG1_On = (1 << 9),
	GFX_BG2_On = (1 << 10),
	GFX_BG3_On = (1 << 11),

	GFX_BG_ALL = (GFX_BG0_On | GFX_BG1_On | GFX_BG2_On | GFX_BG3_On),

	//Sprites
	GFX_OBJ_1D = (1 << 6),
	GFX_OBJ_On = (1 << 12)


}LCDPresets;


//-------------------------------





//------------Background Setup/Functions------------

//not really needed
typedef struct
{
	unsigned short x;
	unsigned short y;
}PACKED BG_Scroll;


//=======================================================
//BG REG summery no$cash
//Bit   Expl.
//0-1   BG Priority           (0-3, 0=Highest)
//2-3   Character Base Block  (0-3, in units of 16 KBytes) (=BG Tile Data)
//4-5   Not used (must be zero)
//6     Mosaic                (0=Disable, 1=Enable)
//7     Colors/Palettes       (0=16/16, 1=256/1)
//8-12  Screen Base Block     (0-31, in units of 2 KBytes) (=BG Map Data)
//13    Display Area Overflow (0=Transparent, 1=Wraparound; BG2CNT/BG3CNT only)
//14-15 Screen Size (0-3)

//Summery of the screen sizes
//Value  Text Mode      Rotation/Scaling Mode
//0      256x256 (2K)   128x128   (256 bytes)
//1      512x256 (4K)   256x256   (1K)
//2      256x512 (4K)   512x512   (4K)
//3      512x512 (8K)   1024x1024 (16K)
//=======================================================


//Background address registers
//these can be accessed as an array
//
// BGREG[0]
// BGREG[1]
// BGREG[2]
// BGREG[3]
#define BGREG ((unsigned short*)0x04000008)

//individual registers for the background
#define BGREG0 *((unsigned short*)0x04000008)
#define BGREG1 *((unsigned short*)0x0400000a)
#define BGREG2 *((unsigned short*)0x0400000c)
#define BGREG3 *((unsigned short*)0x0400000e)

//screen scrolling
//can be sent a struct of type BG_Scroll to perform the movement
//this can also be accessed like an array
#define BGScrollAdd ((BG_Scroll*)0x04000010)

#define BG0 ((unsigned short*)0x04000010)

//Character Blocks
// This is a little odd but it works as follows
// CHAR_BASE_BLOCK(0)[i] = *Data
// this will allow access to different character blocks
// but also at the same time access the data inside as an array
// with 16 only 16bits can be copied over
#define CHAR_BASE_BLOCK16(x) ((unsigned short*)(0x06000000 + ((x) << 14)))
#define CHAR_BASE_BLOCK32(x) ((unsigned int*)(0x06000000 + ((x) << 14)))

//The screen base block works off a similar system but since we use
// 2kb blocks we need to shift the memory by offsets of 2kb
#define SCREEN_BASE_BLOCK16(x) ((unsigned short*)(0x06000000 + ((x) << 11)))
#define SCREEN_BASE_BLOCK32(x) ((unsigned int*)(0x06000000 + ((x) << 11)))

//Background colour pallet address
#define BG_Pallet ((unsigned short*)0x05000000)

//-------------------------------





//------------Sprites------------

// A structure for the sprite attributes
// This can be used to send to OAMAdd in one chunck
// to lower the calls to OAMAdd
typedef struct
{
	unsigned short Att0;
	unsigned short Att1;
	unsigned short Att2;
	unsigned short Att3Padding;
}ALIGN(2) SpriteAtt;

//For Sprites in 32bit addressing
#define OBJAdd32	((unsigned int*)0x06010000)

//For Sprites in 16bit addressing
#define OBJAdd16	((unsigned short*)0x06010000)

//OAM Address
#define OAMAddress	((unsigned short*)0x07000000)


//Spite Pallet
#define OBJPallet	((unsigned short*)0x05000200)

//-------------------------------





//------------Other------------

//used to create a mossiac effect with the different backgrounds
#define MOSAICAdd *((unsigned short*)0x0400004C)
//from no$cash
// The Mosaic function can be separately enabled/disabled for BG0-BG3 by BG0CNT-BG3CNT Registers,
// as well as for each OBJ0-127 by OBJ attributes in OAM memory. Also, setting all of the bits
// below to zero effectively disables the mosaic function.
//
//  Bit   Expl.
//  0-3   BG Mosaic H-Size  (minus 1)
//  4-7   BG Mosaic V-Size  (minus 1)
//  8-11  OBJ Mosaic H-Size (minus 1)
//  12-15 OBJ Mosaic V-Size (minus 1)
//
// Example: When setting H-Size to 5, then pixels 0-5 of each display row are colorized as pixel 0,
// pixels 6-11 as pixel 6, pixels 12-17 as pixel 12, and so on.

#define BLENDAdd *(unsigned short*)0x04000050)

//from no$cash
//	Bit   Expl.
//	0     BG0 1st Target Pixel (Background 0)
//	1     BG1 1st Target Pixel (Background 1)
//	2     BG2 1st Target Pixel (Background 2)
//	3     BG3 1st Target Pixel (Background 3)
//	4     OBJ 1st Target Pixel (Top-most OBJ pixel)
//	5     BD  1st Target Pixel (Backdrop)
//	6-7   Color Special Effect (0-3, see below)
//	0 = None                (Special effects disabled)
//	1 = Alpha Blending      (1st+2nd Target mixed)
//	2 = Brightness Increase (1st Target becomes whiter)
//	3 = Brightness Decrease (1st Target becomes blacker)
//	8     BG0 2nd Target Pixel (Background 0)
//	9     BG1 2nd Target Pixel (Background 1)
//	10    BG2 2nd Target Pixel (Background 2)
//	11    BG3 2nd Target Pixel (Background 3)
//	12    OBJ 2nd Target Pixel (Top-most OBJ pixel)
//	13    BD  2nd Target Pixel (Backdrop)
//	14-15 Not used

//-------------------------------

//------------Time------------
/// <summary>	Counts each frame that has passed. </summary>
extern u32 Frame_Count;
//-------------------------------
// 
////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Maths. </summary>
///
/// <remarks>	Chris, 01/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Macro for setting a BIOS function call. </summary>
//
// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//#ifndef (__thumb__)
//	#define swi_call(x)   asm volatile("swi\t "#x ::: "r0", "r1", "r2", "r3")
//#else
//	#define swi_call(x)   asm volatile("swi\t "#x"<<16" ::: "r0", "r1", "r2", "r3")
//#endif

#endif //End of _BP_TOOLSET_H_
