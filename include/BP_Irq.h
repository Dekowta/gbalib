////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:	Black Paw Interrupt functions
//
// summary:	Controllers some of the Interrupt routines on the GBA
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BP_IRQ_H
#define BP_IRQ_H

#include "BP_ToolSet.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Memory Locations for interrupts. </summary>
///
/// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//The master Interrupt register 
//If this is 1 then Interrupts are enabled. If 0 then none are enabled
#define IRQ_IME  *((unsigned short*)0x04000208)

//The Interrupt Enable register is used to set a 
//specific type of Interrupt as there are 14 different IRQ
#define IRQ_IE *((unsigned short*)0x04000200)

//Interrupt Request Flag
#define IRQ_IF *((unsigned short*)0x04000202)

//Bios Interrupt Request Flag
#define IRQ_IFBIOS *((unsigned short*)0x03007FF8)

//=======================================================
// The IE, IF and IFBIOS should all be laid out as followed 
//summery from no$spec
//  Bit   Expl.							 IE			 IF/IFBIOS
//  0     LCD V-Blank                    (1=Enable)  (1=Request Interrupt)
//  1     LCD H-Blank                    (etc.)      (etc.)
//  2     LCD V-Counter Match            (etc.)      (etc.)
//  3     Timer 0 Overflow               (etc.)      (etc.)
//  4     Timer 1 Overflow               (etc.)      (etc.)
//  5     Timer 2 Overflow               (etc.)      (etc.)
//  6     Timer 3 Overflow               (etc.)      (etc.)
//  7     Serial Communication           (etc.)      (etc.)
//  8     DMA 0                          (etc.)      (etc.)
//  9     DMA 1                          (etc.)      (etc.)
//  10    DMA 2                          (etc.)      (etc.)
//  11    DMA 3                          (etc.)      (etc.)
//  12    Keypad                         (etc.)      (etc.)
//  13    Game Pak (external IRQ source) (etc.)      (etc.)
//  14-15 Not used
//=======================================================

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Type defenitions. </summary>
///
/// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//function pointer that will be used to set the location of a function that will be called with every update
//the desired interrupt
typedef void (*fnptr)(void);
#define IRQ_ISR_MAIN *(fnptr*)(0x03007FFC)

//The different IRQ types and there values 
typedef enum IRQ_Index
{
	IRQ_Vblank = 0,
	IRQ_Hblank = 1,
	IRQ_Vcount = 2,
	IRQ_Timer0 = 3,
	IRQ_Timer1 = 4,
	IRQ_Timer2 = 5,
	IRQ_Timer3 = 6,
	IRQ_Serial = 7,
	IRQ_DMA0   = 8,
	IRQ_DMA1   = 9,
	IRQ_DMA2   = 10,
	IRQ_DMA3   = 11,
	IRQ_Keypad = 12,
	IRQ_Gamepak= 13
}IRQ_Index;



typedef struct IRQ_irs
{
	unsigned int flag;	 //The flag that will be triggered
	fnptr irs;			 //The function pointer
}IRQ_irs;

typedef struct IRQ_Sender
{
	unsigned short IRQ_Reg1Off;	//!< sender reg - REG_BASE
	unsigned short IRQ_Reg2Flag;	//!< irq-bit in sender reg
} ALIGN(4) IRQ_Sender;

extern IRQ_irs IRS_Table[13]; //stores a irs for each irq type


//////////////////////////////////////////////////////////////////////////////////////////////////////
///// <summary>	Intialise the IRQ for vblank mode ONLY. </summary>
/////
///// <remarks>	Chris, 19/05/2012. </remarks>
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//void BP_SetVblankIrq(fnptr Irq_Function);
//
//void BP_SetVblankIrq(fnptr Irq_Function)
//{
//	//since the function wants to create a IRQ need to make sure
//	//that the main IRQ register is to to enable Irq requests
//	IRQ_IME = 1;
//	
//	//since the first address in the irq register is for the vblank irq then
//	//this will need to be one.
//	//Since this function is more of a placeholder there is no need to worry about other
//	//Irq as they should not be used with this function
//	//IF THEY ARE THEN THIS FUNCTION WILL MOST LIKELY CAUSE ISSUES
//	IRQ_IE	= 1;
//
//	//This function will also trigger the BIOS irq flag for the vblank so that the the
//	//system call to wait for the vblank is set for the bios
//	//this is doen when the BP_VBlankWait() function is called
//	IRQ_IFBIOS = 1;
//
//	//The 3rd bit in the display status register needs to be 1 since this function enables
//	//The vblank irq and the display needs to know this
//	GFX_DISPSTAT = (1 << 3);
//
//	//finally since we are also setting a function to be called every frame
//	IRQ_ISR_MAIN = Irq_Function;
//}
//
////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Will call the ARM function swi 5 for setting the vblank wait. </summary>
///
/// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////
void BP_VBlankWait();




//The Following method and functionality has been taken and adapted from toncs gba library 
//I do not take any credit for the following code as well as the BP_Irq.s code
//This is also a lighter version of what is avaliable from toncs own library
//
//I also believe the libgba also uses a similar method for its irq's however tonc did mention
//in his code that it does work differently but I dont know how knows it
//
//A explination of how it all works:
//
//
// 1 - The Master IRQ is set allong with the Master IRS
// 2 - Then the user can set IRQ's and IRS's for the IRQ's via the add IRQ function
// 3 - Once a IRQ has been added it will call the Master IRS even if a IRS for that 
//	   IRQ has been set or not
// 4 - If an IRS has been set for the IRQ then it will be added to the IRS table
//	   The IRS table is sent to the IRS Master and depending on which IRQ has been
//	   Triggered it will activate that correct IRS
// 
//There is also a small trick with using a master irs not only for managing all the irs's but
//also for the format the code is compiled to
//the IRS HAS TO BE ARM code other wise it wont work as the cpu is set to ARM mode when a IRQ is triggered
//that is a problem when all the c code is being compiled to thumb (as its quicker with most of the busses being 16bit)
//however having a Master IRQ and compiling it to arm via ASM will stop this issue and will also provide
//as nifty way of running thumb code during an irq.
//the bx op0 oprtation is used on the function pointer in the IRS table and it kicks the cpu back into
//thumb mode and executes the function.
//Once it comes out of the function it will kick back into ARM mode and carry on as nothing happened
//
//There is also some BIOS stuff in the Master IRS however that will be explained in the ASM file

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	prototype for the master irs that will manage the irs in the table
/// 			This code will be found in BP_IRQ.s. </summary>
///
/// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

IWRAM_CODE void IRS_Master(void);

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Intialise the IRQ and set the pointer to the main IRQ Loop. </summary>
///
/// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_InitIRQ();

////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>	Enable an IRQ
/// 			A irs can be set for the irq however this value
/// 			can be set to NULL. </summary>
///
/// <remarks>	Chris, 19/05/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

void BP_EnableIRQ(enum IRQ_Index IRQ_ID, fnptr isr);

#endif //End of BP_IRQ_H
