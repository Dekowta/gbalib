////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	Black Paw Collision
//
// summary:	some basic collision checking mechanics
//			they are written in asm to provide a fast frame
//			work for collision detection
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef BP_COLLISION_H
#define BP_COLLISION_H

#include "BP_ToolSet.h"

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Collision Type structures. </summary>
//
// <remarks>	Chris, 07/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

//Cube/Rectangle collision type
typedef struct Ccube
{
	unsigned short x;
	unsigned short y;
	unsigned short width;
	unsigned short height;
}Ccube;

//Circle collision type
typedef struct Ccircle
{
	unsigned short x;
	unsigned short y;
	unsigned short Radius;
}ALIGN(4) Ccircle;



//This is most likely the wrong method to do this but I need a 
//data store for the map collision function and with only 8
//registers it starts to get a little cramped
//this struct just stores the x and y of where the collison was triggered
typedef struct	Cpos
{
	unsigned short bottom;
	unsigned short BoolBottom; //not sure what data size a bool is and also to lazy to manage the change in data size 
	unsigned short Top;
	unsigned short BoolTop;
	unsigned short Left;
	unsigned short BoolLeft;
	unsigned short Right;
	unsigned short BoolRight;
}Cpos;

Cpos CollisionPos;

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Collision Detection between two Ccube objects
// 				Uses AABB collision formula and written in ARM Thumb. </summary>
//
// <remarks>	Chris, 07/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

bool BP_CIntersectBox(Ccube* a, Ccube* b);

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Collision Detection between two Ccircle type
// 				objects. </summary>
//
// <remarks>	Chris, 07/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

bool BP_CIntersectCircle(Ccircle* a, Ccircle* b);

////////////////////////////////////////////////////////////////////////////////////////////////////
// <summary>	Collision Detection between The map and a Ccube object. </summary>
//
// <remarks>	Chris, 08/06/2012. </remarks>
////////////////////////////////////////////////////////////////////////////////////////////////////

bool BP_CMapBox(MapAtt* MapData, Ccube* Obj);

#endif